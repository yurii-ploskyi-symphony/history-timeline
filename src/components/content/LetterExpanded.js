import React, {Component} from 'react'
export default class LetterExpanded extends Component {

    constructor(props) {
        super(props);
        this.letter = this.props.letter;
        this.getContent = this.getContent.bind(this);
    }

    getContent() {
        return this.letter.content.map((content, index) => <p key={index} className="contentBlock">{content.text}</p>);
    }

    render() {
        return <div>
            <div>
                <div className="to">Letter {this.letter.letter.addressee}</div>
                < div className="from"> FROM ALBRECHT DURER</div>
            </div>
            {this.getContent()}
        </div>
    }
}