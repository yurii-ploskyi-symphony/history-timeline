import React, {Component} from 'react'

export default class Work extends Component {

    constructor(props) {
        super(props);
        this.work = this.props.event;
    }

    render() {
        return <div style={{left: `${this.props.left}px`, display: this.props.display}} className="work">
            <div className="workBlock">
                <div className="centered-container-left">
                    <div className="workTitle">{this.work.title}</div>
                    <div className="workTechnique">{this.work.work.technique}</div>
                </div>
            </div>
            <div className="imageBlock">
                <div className="imageBlockInner">
                  <div className="workWidth"
                       style={{left: this.props.isCurrent ? '96%' : '90%'}}>{`${(this.work.work.size && this.work.work.size.width) || 29} cm`}</div>
                  <img className="workImage" src={this.work.work.image}></img>
                  <div className="workHeight"
                       style={{top: this.props.isCurrent ? '100%' : '90%'}}>{`${(this.work.work.size && this.work.work.size.height) || 74} cm`}</div>
                </div>

            </div>
            <div className="workBlock">
                <div className="centered-container-right">
                    <div className="workDesc">{this.work.work.description}</div>
                </div>
            </div>
        </div>;
    }
}
