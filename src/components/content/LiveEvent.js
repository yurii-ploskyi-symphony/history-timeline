import React, {Component} from 'react'
export default class LifeEvent extends Component {

    constructor(props) {
        super(props);
        this.lifeEvent = this.props.event;
    }

    render() {
        return <div className="affair" style={{left: `${this.props.left}px`, display:this.props.display}}>
            <div className="affairSign">
                <img src="public/img/lifeEventSign.png"></img>
            </div>
            <div className="affairDetails">
                <div className="centered-container-affair">
                    <span className="workTitle">{this.lifeEvent.title}</span>
                </div>
            </div>
        </div>
    }
}