import React, {Component} from 'react'
import LetterExpanded from "./LetterExpanded";
import  letterToggle from  '../../actions/letterToggle';
import {connect} from 'react-redux';
@connect()
export default class Letter extends Component {

    constructor(props) {
        super(props);
        this.toggleClassName = this.toggleClassName.bind(this);
        this.expand = this.expand.bind(this);
        this.letter = this.props.event;
        this.state = {className: 'letter', expanded: false};
        this.top = 70;
    }

    expand() {
        this.props.dispatch(letterToggle);
        const newState = {...this.state};
        newState.className = this.toggleClassName(this.state.className);
        newState.expanded = !this.state.expanded;
        this.setState(newState);
    }

    toggleClassName(className) {
        return className == 'letter' ? 'letterExpanded' : 'letter';
    }
    getLetterContent() {
        if (this.state.expanded) {
            return <LetterExpanded letter={this.letter}/>
        } else {
            return ( <div >
                <div className="to">Letter {this.letter.letter.addressee}</div>
                < div className="from"> FROM ALBRECHT DURER</div>
            </div>)
        }
    }

    render() {
        return <div className={this.state.className} style={{left: `${this.props.left}px`, display: this.props.display}}
                    onClick={this.expand}>
            <div className={this.state.expanded ? 'letterExpandedContent' : 'letterContent'}>
                {this.getLetterContent()}
            </div>
        </div>
    }
}