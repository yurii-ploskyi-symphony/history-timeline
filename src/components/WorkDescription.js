import React, {Component} from 'react'
export default class WorkDescription extends Component {
    render() {
        return <div className="workDescription">
            <img className="closeSign" src="public/img/closeSign.png"/>
            <div>
                …where he got acquainted with Dürer. To celebrate the emperor and his house, the artist conceived the
                large Triumphal Arch woodcut, for which he was rewarded with 100 yearly florins.
            </div >
        </div>
    }
}