import React, {Component} from "react";
import HeadLine from "./HeadLine";
import Timeline from "./timeline/Timeline";
import getEvents from "../actions/eventsActions";
import EventLine from "./EventLine";

import {connect} from 'react-redux';

@connect((store) => {
    return {
        events: store.events.events,
        currentFrame: store.events.currentFrame,
        changeFrameAllowed: store.frame.changeFrameAllowed,
        width: store.mainReducer.width
    }
})
export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.wheelListener = this.wheelListener.bind(this);
        this.isOutsideFrame = this.isOutsideFrame.bind(this);
        this.setCurrentFrame = this.setCurrentFrame.bind(this);
        this.dispatch = this.dispatch.bind(this);
        this.state = {horizontalPosition: 0};
        this.dispatch({type: 'WIDTH', payload: this.getWidth()});
        this.shift = 2000;
        this.k = 0;
        window.addEventListener('mousewheel', this.wheelListener);
        window.addEventListener('resize', this.resize);
        this.direction = 1;
    }

    resize() {
        if (new Date().getTime() - this.startResize < 5000) {
            return;
        }
        this.startResize = new Date().getTime();
        location.reload();
    }

    setCurrentFrame(number) {
        this.dispatch({type: 'CHANGE_CURRENT_FRAME', payload: number});
    }

    dispatch(action) {
        this.props.dispatch((dispatch) => {
            dispatch(action);
        })
    }

    componentWillMount() {
        this.props.dispatch((dispatch) => {
            getEvents().then((response) => {
                dispatch({type: 'RECEIVED_EVENTS', payload: response.data});
            })
        });
    }

    wheelListener(e) {
        if (new Date().getTime() - this.start < 1350) {
            return;
        }
        this.start = new Date().getTime();
        this.scrollHandler(e);
    }

    scrollHandler(e) {
        if (this.isOutsideFrame(e) || !this.props.changeFrameAllowed) {
            return;
        }
        this.direction = this.iterateCurrentFrame(e);
        const newState = {...this.state};
        newState.horizontalPosition -= this.direction * (this.props.width + this.shift);
        newState.horizo
        this.setState(newState);
    }

    isOutsideFrame(e) {
        return (this.k == 0 && e.deltaY < 0) || (this.k == this.state.framesCount - 1 && e.deltaY > 0);
    }

    iterateCurrentFrame(e) {
        let direction = 1;
        let newCurrentNumber = this.k;
        if (e.deltaY < 0) {
            direction *= -1;
            newCurrentNumber--
        } else {
            newCurrentNumber++
        }
        this.k = newCurrentNumber;
        // this.setCurrentFrame(newCurrentNumber);
        return direction;
    }

    getWidth() {
        return window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;
    }

    render() {
        if (this.props.events) {
            return (
                <div className="outer">
                    <HeadLine/>
                    <EventLine horizontalPosition={this.state.horizontalPosition} currentFrame={this.k}/>
                    <div className="footer">
                        <Timeline horizontalPosition={this.state.horizontalPosition + ((this.props.width) + this.shift)}
                                  currentFrame={this.k}
                                  direction={this.direction}/>
                    </div>
                </div>
            );
        } else {
            return <div>Loading ...</div>
        }
    }
}
