import React, {Component} from 'react'
import Work from "./content/Work";
import Letter from "./content/Letter";
import LifeEvent from "./content/LiveEvent";

import {connect} from 'react-redux';

@connect((store) => {
    return {
        events: store.events.events,
        // currentFrame: store.events.currentFrame,
        width: store.mainReducer.width
    }
})
export default class EventLine extends Component {

    constructor(props) {
        super(props);
    }

    getFrames(events, horizontalPosition, shift) {
        if (!this.props.events) {
            return [];
        }
        return events.map((event, index) => this.getEvent(event, index, horizontalPosition + shift * index));
    };


    getEvent(event, index, left) {
        if (!this.getDisplay(index, this.props.currentFrame)) {
            return;
        }
        switch (event.type) {
            case 'Work':
                return <Work left={left} key={index} event={event} isCurrent={this.props.currentFrame == index}/>
                break;
            case 'Letter':
                return <Letter left={left} key={index}
                               event={event}/>
                break;
            case 'Life Event':
                return <LifeEvent left={left} key={index} event={event}/>
                break;
        }
    }

    getDisplay(index, currentFrame) {
        return (index >= currentFrame - 1 && index <= currentFrame + 1);
    }

    render() {
        return <div>{this.getFrames(this.props.events, this.props.horizontalPosition, this.props.width + 2000)}</div>;
    }
}