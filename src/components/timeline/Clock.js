import React, {Component} from 'react'
import * as d3 from 'd3';

export default class Clock extends Component {

    constructor(props) {
        super(props);
        this.circle = {x: 100, y: 200, radius: 75};
        this.state = {horizontalPosition: this.props.horizontalPosition};
        this.monthNumbers = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII'];
        this.months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        this.getMonthNumber = this.getMonthNumber.bind(this);
        this.viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        this.x = this.viewPortHeight / 18.5;
        this.y = this.viewPortHeight / 100 * 21.5;
        this.clockCircleX = this.viewPortHeight / 10;
        this.clockCircleY = this.viewPortHeight / 5;
        this.clockCircleBigRadius = this.viewPortHeight / 100 * 8;
        this.clockCircleSmallRadius = this.viewPortHeight / 100 * 5;

    }

    checkDiff(firstDate, secondDate) {
        if (secondDate.year > firstDate.year) {
            return 1;
        } else if (secondDate.year < firstDate.year) {
            return -1;
        }

        const secondMonthNumber = this.getMonthNumber(secondDate.month);
        const firstMonthNumber = this.getMonthNumber(firstDate.month);

        if (secondMonthNumber > firstMonthNumber) {
            return 1;
        } else if (secondMonthNumber < firstMonthNumber) {
            return -1;
        }

        if (secondDate.day > firstDate.day) {
            return 1;
        } else if (secondDate.day < firstDate.day) {
            return -1;
        }


        else {
            return 0;
        }
    }


    diff(firstDate, secondDate) {
        switch (this.checkDiff(firstDate, secondDate)) {
            case 0 :
                return 0;
            case 1:
                return this.countDiff(firstDate, secondDate);
            case -1:
                return -1 * this.countDiff(secondDate, firstDate);
        }
    }

    countDiff(earlierDate, laterDate) {
        let integerYears = 0;
        let yearMonths = 0;
        let monthsInCurrentYear = 0;
        if (earlierDate.year != laterDate.year) {
            integerYears = laterDate.year - earlierDate.year - 1;
            yearMonths = 12 - this.getMonthNumber(earlierDate.month);
            monthsInCurrentYear = this.getMonthNumber(laterDate.month);
        } else {
            yearMonths = this.getMonthNumber(laterDate.month) - this.getMonthNumber(earlierDate.month);
        }
        return -(integerYears * 12 + yearMonths + monthsInCurrentYear);
    }

    getMonthNumber(month) {
        this.months.indexOf(month);
    }


    componentDidMount() {
        d3.select('#scale').append('textPath').attr('xlink:href', '#scalePath');

        d3.selectAll('.scale').remove();
        var svg = d3.select('svg')
            .append('g').attr('class', 'scale');

        const step = 2 * Math.PI / 12;

        const r = this.viewPortHeight / 17;
        const f = (d, i) => {
            let startAngle = step * i;
            return d3.arc().innerRadius(r / 2).outerRadius(r).startAngle(startAngle).endAngle(startAngle + step)
        };
        svg.selectAll('.monthArc')
            .data(d3.pie(this.monthNumbers)(this.monthNumbers))
            .enter().append('path')
            .style('stroke', 'none')
            .attr('class', 'monthArc')
            .attr('id', (d, i) => ('monthArc_' + i))
            .attr('d', (d, i) => f(d, i)());

        svg.selectAll('.monthText')
            .data(this.monthNumbers)
            .enter().append('text')
            .attr('class', 'monthText')
            .attr('id', (d, i) => `monthText${i}`)
            .append('textPath')
            .style('text-anchor', 'middle')
            .attr('startOffset', '10%')
            .attr('xlink:href', (d, i) => '#monthArc_' + i)
            .text((d) => d);

        this.rotateNumbers(this.props.currentEvent);

    }

    rotateNumbers(currentEvent, previousEvent) {
        currentEvent = this.props.currentEvent;
        previousEvent = this.props.previousEvent;

        const selectedNumber = this.getMonthNumber(currentEvent.dateFrom.month || currentEvent.dateTo.month);
        if (!previousEvent) {
            let rotateDeg = -30 * (selectedNumber - 11);
            this.previousRad = rotateDeg;
        }
        let diff = 0;
        if (previousEvent) {
            diff = this.diff(previousEvent.dateFrom, currentEvent.dateFrom);
        }

        const leftBound = this.previousRad;
        const right = leftBound + (diff * (30));
        this.previousRad = right;

        const rotTween = () => {
            const i = d3.interpolate(leftBound, right);
            return (t) => "rotate(" + i(t) + ")";
        };

        d3.selectAll('.monthText').transition().duration(1000).style('fill', (d, i) => i == selectedNumber ? '#ffc866' : '#ac8548')
            .style('font-weight', (d, i) => i === selectedNumber ? '900' : '100');
        d3.selectAll('.monthText').transition().duration(1000).attrTween("transform", rotTween).style('fill', (d, i) => i == selectedNumber ? '#ffc866' : '#6C522B')
            .style('font-weight', (d, i) => i === selectedNumber ? '900' : '100');
        this.changeYear(currentEvent.dateFrom.year || currentEvent.dateTo.year);
    }

    getDate(eventDate) {
        return new Date(eventDate.year, this.getMonthNumber(eventDate.month), eventDate.day);
    }

    getMonthNumber(month) {
        const monthNumber = this.months.indexOf(month);
        return monthNumber === -1 ? 1 : monthNumber;
    }

    changeYear(year) {
        this.iterateNumber(".week", year);
    }

    iterateNumber(className, value) {
        var format = d3.format("d");
        d3.select(className)
            .transition()
            .duration(1000)
            .on("start", function repeat() {
                d3.active(this)
                    .tween("text", function () {
                        var that = d3.select(this),
                            i = d3.interpolateNumber(that.text().replace(/,/g, ""), value);
                        return function (t) {
                            that.text(format(i(t)));
                        };
                    })
                    .transition()
                    .delay(1500)
                    .on("start", repeat);
            });
    }

    render() {
        this.rotateNumbers(this.props.currentEvent, this.props.previousEvent);
        return <g className="clock">
            <circle className="clockCircle" cx={this.clockCircleX} cy={this.clockCircleY}
                    r={this.clockCircleBigRadius}/>
            <circle className="clockCircle" cx={this.clockCircleX} cy={this.clockCircleY}
                    r={this.clockCircleSmallRadius}/>
            <text className="week" x={this.x} y={this.y}>
            </text>
        </g>
    }
}