import React, {Component} from 'react';
import * as d3 from "d3";
import {connect} from 'react-redux';
import {getPath, getAllPath} from '../../services/dateService';

@connect((store) => {
    return {
        events: store.events.events,
        width: store.mainReducer.width,
        currentFrameHash: store.events.currentFrameHash,
    }
})
export default class Curve extends Component {

    constructor(props) {
        super(props);
        this.getPathD = this.getPathD.bind(this)
        this.test = this.test.bind(this);
        this.getPoints = this.getPoints.bind(this);
        this.getLabels = this.getLabels.bind(this);
        this.recount = this.recount.bind(this);
        this.getDelta = this.getDelta.bind(this);
        this.getDirection = this.getDirection.bind(this);
        this.lineFunction = d3.line().x((d) => d.x).y((d) => d.y).curve(d3.curveBundle);
        this.recount();
        this.height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        this.delta = this.height / 50;
    }

    recount() {
        this.pathD = this.getPathD();
        const allPath = getAllPath(this.lineData, this.props.events);
        if (allPath) {
            this.lineData = allPath;
        }
        this.getPoints();
        this.getLabels();
    }

    getPathD() {
        this.lineData = getPath(this.props.events, this.props.width);
        return this.lineFunction(this.lineData);//.replace(/,NaN,/g, '').substring(1);
    }


    getPoints() {
        this.points = [];
        this.lineData = this.lineData.sort(function (a, b) {
            return a.x - b.x;
        });
        this.lineData.forEach((data, i) => {
            if (data.city && data.city != 'test') {
                this.points.push(
                    <circle className="point" key={i} cx={data.x} cy={data.y}/>);
            }
        });
        this.setState({points: this.points});
    }

    getLabels() {
        this.labels = [];
        this.lineData = this.lineData.sort(function (a, b) {
            return parseFloat(a.x) - parseFloat(b.x);
        });
        this.lineData.forEach((data, i) => {
            if (data.city && data.city != 'test') {
                this.labels.push(
                    <text opacity={0} key={i} className="curveLabel"
                          x={data.x - this.props.width / 2000 * data.city.length * 4}
                          y={data.y + this.getDelta(i, this.lineData)}>{data.city}</text>);
            }
        });
        this.setState({labels: this.labels});
    }

    getDelta(i, lineData) {
        if (this.getDirection(i, lineData)) {
            return -this.delta;
        } else {
            return this.delta;
        }
    }

    getDirection(i, lineData) {
        if (i == 0) {
            return lineData[i].y < lineData[i + 1].y;
        }
        if (i == lineData.length - 1) {
            return lineData[i].y < lineData[i - 1].y;
        }
        if (lineData[i].y < lineData[i - 1].y && lineData[i].y < lineData[i + 1].y) {
            return true;
        } else {
            return false;
        }
    }

    componentDidMount() {
        this.highLightCurrentPoint();
        this.recount();
    }


    test() {
        const data = [];
        const mainCurveNode = d3.select('#mainCurve').node();
        const startPoint = this.lineData[this.props.currentFrame - this.props.direction * 1];
        const startLength = startPoint && startPoint.length;
        const endLength = this.lineData[this.props.currentFrame].length;


        let currentLength = startLength;
        const isNotFinished = (direction, currentLength, endLength) => {
            if (direction > 0) {
                return currentLength < endLength;
            } else {
                return currentLength > endLength;
            }
        };
        while (isNotFinished(this.props.direction, currentLength, endLength)) {
            data.push(mainCurveNode.getPointAtLength(currentLength));
            currentLength += this.props.direction * 2;
        }
        if (!data.length) {
            const point = this.lineData[this.props.currentFrame];
            const pointAtLength = mainCurveNode.getPointAtLength(point.length);
            data.push(pointAtLength);
            data.push(pointAtLength);
        }


        let svg = d3.select('.pointerMove');
        if (!svg._groups[0][0]) {
            svg = d3.select("svg").append('g').attr('class', 'pointerMove');//.attr('transform', 'translate(0,23vh)');
        }
        let path = svg.select('#pointPath');

        if (!path._groups[0][0]) {
            path = svg.append("path").attr('class', 'curve').attr('id', 'pointPath').style('stroke', 'none');
        }

        path.attr("d", this.lineFunction(data));

        let circle = svg.select('#pointCircle');

        if (!circle._groups[0][0]) {
            circle = svg.append("circle").attr('id', 'pointCircle')
                .attr("r", this.height / 250);
        }


        function transition() {
            try {
                circle.transition()
                    .duration(1000)
                    .attrTween("transform", translateAlong(path.node()))
                    .each("end", transition);
            } catch (err) {
                // console.log(err);
            }
        }

        transition();

        function translateAlong(path) {
            var l = path.getTotalLength();
            return function (d, i, a) {
                return function (t) {
                    var p = path.getPointAtLength(t * l);
                    return "translate(" + (p.x) + "," + (p.y) + ")";
                };
            };
        }
    }

    highLightCurrentPoint(currentFrame = 0) {
        d3.selectAll('.curveLabel').style('opacity', (d, i) => i == currentFrame ? '1' : '0');
    }

    render() {
        setTimeout(this.test);
        setTimeout(() => {
            this.highLightCurrentPoint(this.props.currentFrame)
        });
        return <g>
            {this.state && this.state.labels}
            <path className="curve" d={this.pathD} id='mainCurve'>
            </path>
            {this.state && this.state.points}
        </g>;
    }
}

