import React, {Component} from "react";
import Clock from "./Clock";
import Curve from "./Curve";

import {connect} from 'react-redux';
@connect((store) => {
    return {
        events: store.events.events,
        width: store.mainReducer.width
    }
})
export default class Timeline extends Component {

    render() {
        return <svg>
            <g className="curveContainer">
                <Curve currentFrame = {this.props.currentFrame} direction = {this.props.direction}/>
            </g>
            <Clock className="clock" horizontalPosition={this.props.horizontalPosition}
                   currentEvent={this.props.events[this.props.currentFrame]}
                   previousEvent={this.currentFrame === 0 ? null : this.props.events[this.props.currentFrame - this.props.direction]}/>
        </svg>
    }
}