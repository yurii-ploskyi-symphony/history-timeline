import * as d3 from "d3";

const getMaxDate = (events) => {
    return events.reduce((prev, current) => {
        return (prev.dateFrom.year > current.dateFrom.year) ? prev : current
    });
};

const viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
const viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
const getAllEventsWithCities = (events) => {
    return events.filter(event => event.city);
};


const getMinDate = (events) => {
    return events.reduce((prev, current) => {
        return (prev.dateFrom.year < current.dateFrom.year) ? prev : current
    });
};

const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

const format = (n) => {
    if (n === '') {
        return '01';
    }
    return n > 9 ? "" + n : "0" + n;
}

const getTime = (date) => {
    let month = months.indexOf(date.month) + 1;
    if (month === 0) {
        month = "";
    }
    return new Date(`${date.year}/${format(month)}/${format(date.day)}`).getTime();
}

const offset = viewportHeight / 4.5;
const getPoint = (event, horizontalScale) => {
    return {x: horizontalScale(getTime(event.dateFrom)), y: getY(event), city: getCity(event)}
};

const getY = (event) => {
    return (event.city && -viewportHeight / 1000 * event.city.length) || 0;
};

const getCity = (event) => {
    return (event.city && event.city.name) || 'OK';
};

const addLine = (result, leftX, rightX) => {
    const length = rightX - leftX;
    addStraightLine(result, leftX, length);
    result.push({x: rightX, y: 0, city: 'test'});
};

const addStraightLine = (result, previousPointX, length) => {
    result.push({x: previousPointX, y: 0, city: 'test'});
    result.push({x: previousPointX + length, y: 0, city: 'test'});
};

const addIntermediateStraightLine = (result, currentPointX, previousPointX) => {
    addLine(result, previousPointX, currentPointX);
};

const addFinalStraightLine = (result, horizontalScale, previousPointX) => {
    const maxX = horizontalScale(maxTime);
    addLine(result, previousPointX, maxX);
};

let maxTime;
let minTime;
let minDate;
const getPath = (events, width) => {
    maxTime = getTime(getMaxDate(events).dateFrom);
    minDate = getMinDate(events).dateFrom;
    minTime = getTime(minDate);


    const horizontalScale = d3.scaleLinear()
        .domain([minTime, maxTime])
        .range([offset, width - width / 100]);
    const allEventsWithCities = getAllEventsWithCities(events);

    const result = [];
    result.push({x: horizontalScale(minTime), y: 0, city: 'test'});


    let previousPoint;
    allEventsWithCities.forEach((event) => {

        const point = getPoint(event, horizontalScale);
        const hillWidth = viewportWidth / 1700 * event.city.length;

        const currentPointX = point.x - hillWidth / 2;

        if (!previousPoint) {
            previousPoint = {x: offset, y: 0};
        }
        addIntermediateStraightLine(result, currentPointX, previousPoint.x);
        result.push(point);
        previousPoint = {x: point.x + hillWidth / 2, y: 0, city: 'test'};
        result.push(previousPoint);
    });
    addFinalStraightLine(result, horizontalScale, previousPoint.x);
    return result;
};

const getAllPath = (points, events) => {

    const node = d3.select('#mainCurve').node();

    if (!node) {
        return;
    }

    const horizontalScale = d3.scaleLinear()
        .domain([minTime, maxTime])
        .range([0, node.getTotalLength()]);

    const result = [];

    events.forEach(event => {
        const length = horizontalScale(getTime(event.dateFrom));
        const point = node.getPointAtLength(length);
        let age = 0;
        if (minTime && event.dateFrom) {
            age = event.dateFrom.year - minDate.year;
        }
        result.push({
            x: point.x,
            y: point.y,
            city: age.toString() + ' years',
            length: length
        });
    });

    return result;

};
export {getPath, getAllPath}
