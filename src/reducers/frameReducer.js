export default (state = {changeFrameAllowed: true}, action) => {
    if (action.type === 'LETTER_TOGGLE') {
        state = {...state};
        state.changeFrameAllowed = !state.changeFrameAllowed;
    }
    return state;
}