const compare = (a, b) => {
    if (a.dateFrom.year < b.dateFrom.year) {
        return -1;
    }
    if (a.dateFrom.year > b.dateFrom.year) {
        return 1;
    }

    if (a.dateFrom.month < b.dateFrom.month) {
        return -1;
    }
    if (a.dateFrom.month > b.dateFrom.month) {
        return 1;
    }

    if (a.dateFrom.day < b.dateFrom.day) {
        return -1;
    }
    if (a.dateFrom.day > b.dateFrom.day) {
        return 1;
    }
    return 0;
};
export default (state = {currentFrame: 0}, action) => {
    if (action.type === 'RECEIVED_EVENTS') {
        state = {...state};
        state.events = action.payload.sort(compare);
    }
    if (action.type === 'CHANGE_CURRENT_FRAME') {
        state = {...state};
        state.currentFrame = action.payload;
    }
    return state;
}