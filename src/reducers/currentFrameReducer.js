export default (state = {currentFrame: 0}, action) => {
    if (action.type === 'CHANGE_CURRENT_FRAME') {
        state = {...state};
        state.currentFrame = action.payload;
    }
    return state;
}