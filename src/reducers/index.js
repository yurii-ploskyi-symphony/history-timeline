import {combineReducers} from 'redux';
import eventsReducer from './eventsReducer';
import frameReducer from './frameReducer';
import currentFrameReducer from './currentFrameReducer';
import mainReducer from './mainReducer';


const rootReducer = combineReducers({
    events: eventsReducer,
    frame: frameReducer,
    mainReducer: mainReducer
});

export default rootReducer;
