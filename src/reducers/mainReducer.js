export default (state={}, action) => {
    if (action.type === 'WIDTH') {
        state = {...state};
        state.width = action.payload;
    }
    return state;
}