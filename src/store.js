import React, {Component} from 'react';
import {createStore, applyMiddleware} from  "redux";
import {logger} from "redux-logger";
import reducers from './reducers';
import thunk from 'redux-thunk';

const middleware = applyMiddleware(thunk);
const createStoreWithMiddleware = applyMiddleware()(createStore);

export default createStoreWithMiddleware(reducers, middleware);