var express = require('express');
var app = express();
var cors = require('cors');
var path = require('path');
app.use(express.static(__dirname + '/'));

app.use(cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
};



app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
});

app.use(allowCrossDomain);
// app.get('/events', function( req, res, next ) { path.resolve('events.json').json});
app.listen(process.env.PORT || 8080);
var allowedOrigins = ['http://localhost:8080',
    'http://yourapp.com' ];
app.use(cors({
    origin: function(origin, callback){
        // allow requests with no origin
        // (like mobile apps or curl requests)
        if(!origin) return callback(null, true);
        if(allowedOrigins.indexOf(origin) === -1){
            var msg = 'The CORS policy for this site does not ' +
                'allow access from the specified Origin.';
            return callback(new Error(msg), false);
        }
        return callback(null, true);
    }
}));